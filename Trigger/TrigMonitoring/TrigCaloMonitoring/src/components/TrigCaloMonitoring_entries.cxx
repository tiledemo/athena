/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "../HLTCalo_L2CaloEMClustersMonitor.h"
#include "../HLTCalo_TopoCaloClustersMonitor.h"
#include "../HLTCaloClusterTool.h"
#include "../HLTCaloESD_xAODTrigEMClusters.h"
#include "../HLTCaloESD_xAODCaloClusters.h"
#include "../HLTCaloESD_CaloCells.h"


DECLARE_COMPONENT( HLTCalo_L2CaloEMClustersMonitor )
DECLARE_COMPONENT( HLTCalo_TopoCaloClustersMonitor )
DECLARE_COMPONENT( HLTCaloClusterTool )
DECLARE_COMPONENT( HLTCaloESD_xAODTrigEMClusters )
DECLARE_COMPONENT( HLTCaloESD_xAODCaloClusters )
DECLARE_COMPONENT( HLTCaloESD_CaloCells )

